from django.contrib import admin

from .models import Account, FacebookBusinessAccounts, InstagramAccount, InstagramMedia, Updater, Client, Message

class AccountAdmin(admin.ModelAdmin):
	list_display = ('chat_id','access_token')

class FacebookBusinessAccountsAdmin(admin.ModelAdmin):
	list_display = ('name', 'account_id')

class InstagramAccountAdmin(admin.ModelAdmin):
	list_display = ('username','name')

class InstagramMediaAdmin(admin.ModelAdmin):
	list_display = ('media_id', 'comments_count')

class MediaCommentsAdmin(admin.ModelAdmin):
	list_display = ('text','comment_id')

class UpdaterAdmin(admin.ModelAdmin):
	list_display = ('name','updated_at')

class ClientAdmin(admin.ModelAdmin):
	list_display = ('username','lead')


# Register your models here.
admin.site.register(Account, AccountAdmin)
admin.site.register(FacebookBusinessAccounts,FacebookBusinessAccountsAdmin)
admin.site.register(InstagramAccount, InstagramAccountAdmin)
admin.site.register(InstagramMedia, InstagramMediaAdmin)
admin.site.register(Updater, UpdaterAdmin)
admin.site.register(Client,ClientAdmin)
admin.site.register(Message)