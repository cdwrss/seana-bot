# -*- coding: utf8 -*-

from django.urls import re_path
from django.urls import path, include

from .views import CommandReceiveView, FacebookAuthView, SaveUserView, SaveClientView

urlpatterns = [
    re_path(r'^bot/(?P<bot_token>.+)/$', CommandReceiveView.as_view(), name='command'),
    path('login/', FacebookAuthView.as_view()),
    path('save_user/', SaveUserView.as_view()),
    path('save_client/', SaveClientView.as_view())
]
