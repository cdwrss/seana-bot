# Generated by Django 2.0.2 on 2018-04-19 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chat_id', models.CharField(max_length=64)),
                ('access_token', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Accounts',
            },
        ),
    ]
