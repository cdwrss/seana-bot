# Generated by Django 2.0.2 on 2018-04-19 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_auto_20180419_1628'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='chat_id',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
