# Generated by Django 2.0.2 on 2018-04-26 12:49

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0017_auto_20180426_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='updater',
            name='chat_id',
            field=models.CharField(default=None, max_length=64),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='updater',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 26, 18, 48, 57, 703754)),
        ),
    ]
