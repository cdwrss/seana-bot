from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from .models import Client, Message

class ClientRequestSerializer(serializers.Serializer):
    username = serializers.CharField()
    instagram_page = serializers.CharField()
    lead = serializers.BooleanField()

class MessageRequestSerilizer(serializers.Serializer):
    user = ClientRequestSerializer(many=True)
    text = serializers.CharField()
    timestamp = serializers.DateTimeField()
    comment_id = serializers.IntegerField()





