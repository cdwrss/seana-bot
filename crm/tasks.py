import datetime
from celery import task
import collections
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
import requests
import logging
from django.conf import settings
from .models import InstagramMedia, MediaComments
import telepot
from telepot.namedtuple import CallbackQuery
from .models import Account, FacebookBusinessAccounts, InstagramAccount, InstagramMedia, MediaComments, Updater
from urllib.parse import urlencode

from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from django.core.cache import cache
import pytz

from django.template.loader import render_to_string


TelegramBot = telepot.Bot(settings.TELEGRAM_BOT_TOKEN)

@task
def check_last_message():
    accounts = Account.objects.all()
    for account in accounts:
        try:
            id = cache.get(account.chat_id)
            facebooks = FacebookBusinessAccounts.objects.filter(account=account)
            for facebook in facebooks:
                for instagram in InstagramAccount.objects.filter(account=facebook):
                    print(instagram.username)
                    for media in InstagramMedia.objects.filter(instagram=instagram):
                        print(media)
                        link = 'https://graph.facebook.com/v2.11/{}'.format(media.media_id)
                        params = collections.OrderedDict([
                            ('access_token', account.access_token),
                            ('fields', 'comments.limit(10){username,text,timestamp,id}')
                        ])
                        response = requests.get(link, params=urlencode(params))
                        time.sleep(5000)
                        for comment in response.json()['comments']['data']:
                            dt = comment['timestamp'].split('T')
                            date = datetime.datetime.strptime("{0} {1}".format(dt[0],dt[1][0:8]), '%Y-%m-%d %H:%M:%S')
                            now_aware = pytz.utc.localize(date)
                            last_updated_time = Updater.objects.filter(chat_id=account.chat_id).first().updated_at
                            if(last_updated_time < now_aware):

                                if instagram.username != comment['username']:

                                    url = "https://www.instagram.com/{}".format(comment["username"])
                                    client_url = "http://127.0.0.1:8000/planet/save_client/?username={0}&instagram_page={1}&lead={2}&text='{3}'&timestamp={4}&id={5}".format(
                                        comment["username"], "https://www.instagram.com/{}".format(comment["username"]), False, comment['text'], comment['timestamp'], comment['id']
                                    )
                                    context = {
                                        "message": comment['text'],
                                        "username": comment['username']
                                    }

                                    answer = render_to_string('lead.html', context)


                                    TelegramBot.sendMessage(account.chat_id, answer, parse_mode="HTML", reply_markup=InlineKeyboardMarkup(
                                        inline_keyboard=[
                                            [InlineKeyboardButton(text='Перейти на страницу клиента', callback_data='press', url=url)],
                                            [InlineKeyboardButton(text='Это клиент', callback_data='press', url=client_url)]
                                        ]
                                    ))
        except Exception as e:
            print(e)

    Updater.objects.update_or_create(
        chat_id=account.chat_id,
        defaults={
            'updated_at': timezone.now(),
            'name': "Comments"
        }
    )



    return cache.get(account.chat_id)






