from django.db import models
import datetime

# Create your models here.
class Account(models.Model):
    chat_id = models.CharField(max_length=64,unique=True)
    access_token = models.CharField(max_length=255)
    updated_at = models.DateField(null=True)
    class Meta:
        verbose_name_plural = "Accounts"

    def __str__(self):
        return '{}'.format(self.chat_id)

class FacebookBusinessAccounts(models.Model):
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        null=False,
        related_name='business_accounts'
    )
    access_token = models.CharField(max_length=255)
    name = models.CharField(max_length=75)
    business_account_id = models.IntegerField(unique=True)
    connected_instagram_account = models.IntegerField(null=True)
    instagram_business_account = models.IntegerField(null=True)


class InstagramAccount(models.Model):
	account = models.ForeignKey(
		FacebookBusinessAccounts,
		on_delete=models.CASCADE,
		null=False,
		related_name='facebook_accounts'
	)
	instagram_id = models.CharField(max_length=255, unique=True)
	name = models.CharField(max_length=255)
	profile_picture_url = models.CharField(max_length=255)
	biography = models.TextField()
	followers_count = models.IntegerField()
	follows_count = models.IntegerField()
	media_count = models.IntegerField()
	username = models.CharField(max_length=64)



class InstagramMedia(models.Model):
    instagram = models.ForeignKey(
		InstagramAccount,
		on_delete=models.CASCADE,
        null=False,
        related_name='media'
    )
    media_url = models.CharField(max_length=255)
    comments_count = models.IntegerField()
    permalink = models.CharField(max_length=255)
    media_id = models.CharField(max_length=255, unique=True, null=True)

class MediaComments(models.Model):
    media = models.ForeignKey(
        InstagramMedia,
        on_delete=models.CASCADE,
        null=False,
        related_name='comments'
    )
    timestamp = models.DateTimeField()
    text = models.TextField()
    comment_id = models.CharField(max_length=255,unique=True)
    user_link = models.CharField(max_length=255)

class Updater(models.Model):

    name = models.CharField(max_length=32)
    updated_at = models.DateTimeField(default=datetime.datetime.now())
    chat_id = models.CharField(max_length=64, unique=True)

    def update_time(self):
        self.updated_at = datetime.datetime.now()
        self.save(update_fields=['updated_at'])

    def __str__(self):
        return self.name


class Client(models.Model):
    username = models.CharField(max_length=255,unique=True)
    instagram_page = models.CharField(max_length=64)
    phone = models.CharField(max_length=255)
    lead = models.BooleanField(default=False)

class Message(models.Model):
    user = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        null=True,
        related_name='messages')
    text = models.CharField(max_length=255)
    timestamp = models.DateTimeField()
    comment_id = models.IntegerField()
