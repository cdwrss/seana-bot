
# -*- coding: utf8 -*-

import json
import logging

import telepot
from telepot.namedtuple import CallbackQuery
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import requests

from .serializers import ClientRequestSerializer, MessageRequestSerilizer

from django.http import HttpResponseForbidden, HttpResponseBadRequest, JsonResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response

from django.utils import timezone

import collections
from urllib.parse import urlencode

from django.template.loader import render_to_string

from .models import Account, FacebookBusinessAccounts, InstagramAccount, InstagramMedia, MediaComments, Updater, Message

import datetime

from django.core.cache import cache

from .models import Client

# def _display_help(chat_id):

#     help_text = get_template('help.md')
#     return help_text.render()


# def _display_planetpy_feed(chat_id):
#     template = get_template('feed.md')
#     return template.render()

def _display_login(chat_id):
    url = "https://www.facebook.com/v2.12/dialog/oauth?client_id=216990465513037&redirect_uri=https://www.seana.ml/planet/login/&state={}".format(
        chat_id)
    answer = render_to_string('start.html', context={'url': url})
    return answer

def _display_login_failed(chat_id):
    answer = render_to_string('login_failed.html')
    return answer


def seana(chat_id):

    TelegramBot.sendMessage(chat_id, "Пожалуйста подождите... Идет синхронизанция данных...")

   
    update_accounts(chat_id)
    


    return "Синхронизация окончена"



def update_accounts(chat_id):

    url = 'https://graph.facebook.com/v2.11/me/accounts'
    account = Account.objects.get(chat_id=chat_id)

    params = collections.OrderedDict([
        ('access_token', account.access_token),
        ('fields','connected_instagram_account,instagram_business_account,name,access_token')
    ])

    response = requests.get(url, params=urlencode(params))
    time.sleep(5000)
    print(response)

    accounts_data = response.json()["data"]

    for facebook_account in accounts_data:

        try:
            connected_instagram_account = facebook_account['connected_instagram_account']['id']
        except Exception as e:
            connected_instagram_account = None

        try:
            instagram_business_account = facebook_account['instagram_business_account']['id']
        except Exception as e:
            instagram_business_account = None


        FacebookBusinessAccounts.objects.update_or_create(
            business_account_id = facebook_account['id'],
            defaults={
                'access_token': facebook_account['access_token'],
                'name': facebook_account['name'],
                'account': account,
                'connected_instagram_account': connected_instagram_account,
                'instagram_business_account': instagram_business_account
            }
        )


        update_instagram(connected_instagram_account,account,
                         FacebookBusinessAccounts.objects.filter(connected_instagram_account=connected_instagram_account).first())

    data = {
        "chat_id": chat_id,
        "account_id": account.id
    }
    Updater.objects.update_or_create(
        chat_id=chat_id,
        defaults={
            'updated_at': timezone.now(),
            'chat_id':chat_id
        }
    )

    cache.set(chat_id, data, timeout=None)


def update_instagram(id,account, fb):

    if id and fb:
        request = 'https://graph.facebook.com/v2.11/{}'.format(id)
        params = collections.OrderedDict([
            ('access_token', fb.access_token),
            ('fields', 'name,profile_picture_url,biography,followers_count,follows_count,media_count,username')
        ])
        response = requests.get(request, params=urlencode(params))
        time.sleep(5000)
        instagram = response.json()

        InstagramAccount.objects.update_or_create(
            instagram_id = id,
            defaults={
                'name': instagram['name'],
                'profile_picture_url': instagram['profile_picture_url'],
                'biography': instagram['biography'],
                'followers_count': instagram['followers_count'],
                'follows_count': instagram['follows_count'],
                'media_count': instagram['media_count'],
                'username': instagram['username'],
                'account': fb
            }
        )



        upload_link = 'https://graph.facebook.com/v2.11/{}'.format(id)

        update_media(upload_link, account=account, instagram=InstagramAccount.objects.filter(instagram_id=id).first(),state=False)

def update_media(upload_link,account,instagram,state):

    if state:
        response = requests.get(upload_link)
        medias = response.json()["data"]
        premedia = response.json()
    else:
        params = collections.OrderedDict([
            ('access_token', account.access_token),
            ('fields', 'media.limit(10){media_url,comments_count,permalink}, media_count')
        ])
        response = requests.get(upload_link, params=urlencode(params))
        time.sleep(5000)
        premedia = response.json()['media']
        medias = premedia["data"]


    for media in medias:
        InstagramMedia.objects.update_or_create(
            media_id=media["id"],
            defaults={
                'media_url': media['media_url'],
                'instagram': instagram,
                'comments_count': media['comments_count'],
                'permalink': media['permalink'],
            }
        )

        # update_comments(InstagramMedia.objects.filter(media_id=media['id']).first(), account, media['comments_count'])

    try:
        print('updating')
        state = True
        next_link = premedia['paging']['next']
        update_media(next_link, account, instagram,state)

    except Exception as e:
        print("no paging")




def update_comments(media,account,comments_count):
    if int(comments_count) > 0:

        request = 'https://graph.facebook.com/v2.11/{}'.format(media.media_id)
        params = collections.OrderedDict([
            ('access_token', account.access_token),
            ('fields', 'comments.limit(10){username,timestamp,text,id}')
        ])
        response = requests.get(request, params=urlencode(params))
        time.sleep(5000)
        try:
            comments = response.json()["comments"]["data"]
            for comment in comments:
                MediaComments.objects.update_or_create(
                    comment_id=comment['id'],
                    defaults={
                        'text': comment['text'],
                        'timestamp': comment['timestamp'],
                        'media': media,
                        'user_link': "https://www.instagram.com/{}".format(comment)
                    }
                )
        except Exception as e:
            pass


TelegramBot = telepot.Bot(settings.TELEGRAM_BOT_TOKEN)

logger = logging.getLogger('telegram.bot')


class CommandReceiveView(View):
   
    def post(self, request, bot_token):
        if bot_token != settings.TELEGRAM_BOT_TOKEN:
            print("*****")
            print(settings.TELEGRAM_BOT_TOKEN)
            print("*****")
            return HttpResponseForbidden('Invalid token')

        commands = {
            '/start': _display_login,
            '/seana': seana,
        }

        raw = request.body.decode('utf-8')
        logger.info(raw)

        try:
            payload = json.loads(raw)
        except ValueError:
            return HttpResponseBadRequest('Invalid request body')

        else:
            chat_id = payload['message']['chat']['id']
            cmd = payload['message'].get('text')  # command

            has_command = commands.get(cmd.split()[0].lower())
            command = cmd.split()[0].lower()

            account = Account.objects.filter(chat_id=chat_id).first()


            if has_command:

                if command == "/start" and account is None:
                    url = "https://www.facebook.com/v2.12/dialog/oauth?client_id=216990465513037&redirect_uri=https://www.seana.ml/planet/login/&state={}".format(
                        chat_id)
                    keyboard = InlineKeyboardMarkup(
                        inline_keyboard=[
                            [InlineKeyboardButton(text="Зарегистрироваться", url=url)]
                        ]
                    )
                    TelegramBot.sendMessage(chat_id, _display_login(chat_id), parse_mode='HTML', reply_markup=keyboard)


                elif account:

                    if command == "/seana":
                        TelegramBot.sendMessage(chat_id, seana(chat_id))

                else:

                    url = "https://www.facebook.com/v2.12/dialog/oauth?client_id=216990465513037&redirect_uri=https://www.seana.ml/planet/login/&state={}".format(
                        chat_id)

                    TelegramBot.sendMessage(chat_id,_display_login_failed(chat_id), parse_mode='HTML',reply_markup=InlineKeyboardMarkup(
                        inline_keyboard=[
                            [InlineKeyboardButton(text='Зарегистрироватся', callback_data='press', url=url)],
                        ]
                    ))

            else:

                # account = Account.objects.get(chat_id=chat_id)

                # if '>' in command:
                #     business_accounts = account.business_accounts.all()

                #     for i in business_accounts:
                #         if i.name.lower().replace(' ','') in command:
                #             TelegramBot.sendMessage(chat_id, _display_business_account(chat_id, i),reply_markup=ReplyKeyboardMarkup(
                #             keyboard=[
                #                 [KeyboardButton(text="instagram"), KeyboardButton(text="exit")]
                #             ]
                #         ))
                # else:
                TelegramBot.sendMessage(chat_id, "Sorry I don't understand")

            # if func:
            #     if account is None:
            #         TelegramBot.sendMessage(chat_id, func(chat_id), parse_mode='Markdown',reply_markup=ReplyKeyboardMarkup(
            #                         keyboard=[
            #                             [KeyboardButton(text="/start")]
            #                         ]
            #                     ))
            #     else:
            #         if cache.get(chat_id) == "accounts":
            #             TelegramBot.sendMessage(chat_id, func(chat_id), parse_mode='Markdown',
            #                                     reply_markup=ReplyKeyboardMarkup(
            #                                         keyboard=[
            #                                             [KeyboardButton(text="Instagram"),KeyboardButton(text="Exit")]
            #                                         ]
            #                                     ))
            #         if cache.get(chat_id) is None:
            #             TelegramBot.sendMessage(chat_id, func(chat_id), parse_mode='Markdown',reply_markup=ReplyKeyboardMarkup(
            #                         keyboard=[
            #                             [KeyboardButton(text="Me"), KeyboardButton(text="Nado")]
            #                         ]
            #                     ))
            #
            # else:
            #
            #     if cache.get(chat_id) == "accounts":
            #         TelegramBot.sendMessage(chat_id, "Я вас не понимаю",
            #                                 reply_markup=ReplyKeyboardMarkup(
            #                                     keyboard=[
            #                                         [KeyboardButton(text="Instagram"), KeyboardButton(text="Exit")]
            #                                     ]
            #                                 ))
            #     if cache.get(chat_id) is None:
            #         TelegramBot.sendMessage(chat_id, "Я вас не понимаю",
            #                                 reply_markup=ReplyKeyboardMarkup(
            #                                     keyboard=[
            #                                         [KeyboardButton(text="Me"), KeyboardButton(text="Nado")]
            #                                     ]
            #                                 ))

        return JsonResponse({}, status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CommandReceiveView, self).dispatch(request, *args, **kwargs)


class FacebookAuthView(APIView):
    def get(self,request):
        print("***********")
        code = request.GET['code']
        state = request.GET['state']

        params = collections.OrderedDict([
            ('code', code), ('client_id', '216990465513037'),
            ('redirect_uri', 'https://www.seana.ml/planet/login/'),
            ('client_secret', '5ec936402bcd0f27d0cc1aa06359bfc2')
        ])

        r = requests.get('https://graph.facebook.com/v2.12/oauth/access_token?', params=urlencode(params))

        try:
            print(r.json())
            access_token = r.json()['access_token']
            chat_id = state
            Account.objects.update_or_create(
                defaults={
                    'chat_id': chat_id, 
                    'access_token': access_token,
                    'updated_at': datetime.datetime.now()
                }
            )




            keyboard = ReplyKeyboardMarkup(keyboard=[
                [KeyboardButton(text="/seana")]
            ])
            TelegramBot.sendMessage(chat_id, "Вы успешно зарегистрировались", reply_markup=keyboard)
            return Response("welcome")
        except Exception as e:
            print(e)
            return Response("error")

        return render(request, 'authsuccess.html', {})


class SaveUserView(APIView):
    def get(self,request):
        print(request)


class SaveClientView(APIView):
    def get(self,request):
        try:
            client = Client.objects.get_or_create(
                username=request.GET['username'],
                defaults={
                    'instagram_page': request.GET['instagram_page'],
                    'lead': request.GET['lead']
                }
            )

            message = Message.objects.create(
                user=client,
                text=request.GET['text'],
                timestamp=request.GET['timestamp'],
                comment_id=request.GET['id']
            )
            return Response(data={
                "message": "Client saved"
            })
        except Exception as e:
            return Response(data={
                "message": e
            })
        return render(request, 'authsuccess.html', {})




